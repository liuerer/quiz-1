package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class GuessNumberTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	void should_creat_game() throws Exception {
		mockMvc.perform(post("/api/games"))
			.andExpect(status().isCreated());
	}
	
	@Test
	void should_creat_game_with_header_and_id() throws Exception {
		mockMvc.perform(post("/api/games"))
			.andExpect(status().isCreated())
			.andExpect(header().exists("Location"));
	}
	
	@Test
	void should_get_status_of_game() throws Exception {
		mockMvc.perform(post("/api/games"))
			.andExpect(status().isCreated());
		
		mockMvc.perform(get("/api/games/0"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id").exists());
	}
	
	@Test
	void should_get_not_found_error_when_without_id() throws Exception {
		mockMvc.perform(get("/api/games/2"))
			.andExpect(status().isNotFound());
	}
	
	@Test
	void should_response_to_patch() throws Exception {
		mockMvc.perform(post("/api/games"))
			.andExpect(status().isCreated());
		
		String requestStr = new ObjectMapper().writeValueAsString(new GameAnswer("1234"));
		
		MvcResult mvcResult = mockMvc.perform(patch("/api/games/0")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(requestStr))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();
		
		String responseStr = mvcResult.getResponse().getContentAsString();
		ResponseResult responseResult = new ObjectMapper().readValue(responseStr, ResponseResult.class);
		
		if (responseResult.isCorrect()) {
			assertEquals("4A0B", responseResult.getHint());
		} else {
			assertNotEquals("4A0B", responseResult.getHint());
		}
	}
	
	@Test
	void should_response_true_with_correct_answer() throws Exception {
		mockMvc.perform(post("/api/games"))
			.andExpect(status().isCreated());
		
		MvcResult correctResult = mockMvc.perform(get("/api/games/0"))
			.andExpect(status().isOk())
			.andReturn();
		
		String correctStr = correctResult.getResponse().getContentAsString();
		NumberGameInfo numberGameInfo = new ObjectMapper().readValue(correctStr, NumberGameInfo.class);
		
		String requestStr = new ObjectMapper().writeValueAsString(new GameAnswer(numberGameInfo.getAnswer()));
		
		MvcResult mvcResult = mockMvc.perform(patch("/api/games/0")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(requestStr))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();
		
		String responseStr = mvcResult.getResponse().getContentAsString();
		ResponseResult responseResult = new ObjectMapper().readValue(responseStr, ResponseResult.class);
		
		assertEquals("4A0B", responseResult.getHint());
	}
	
	@Test
	void should_get_404_status_code() throws Exception {
		String requestStr = new ObjectMapper().writeValueAsString(new GameAnswer("1234"));
		mockMvc.perform(patch("/api/games/2")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(requestStr))
			.andExpect(status().isNotFound());
	}
	
	@Test
	void should_get_400_status_code() throws Exception {
		String requestStr = new ObjectMapper().writeValueAsString(new GameAnswer("12xy"));
		mockMvc.perform(patch("/api/games/2")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(requestStr))
			.andExpect(status().isBadRequest());
	}
}

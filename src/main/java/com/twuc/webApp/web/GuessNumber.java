package com.twuc.webApp.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

@RestController
public class GuessNumber {
	
	private Map<Integer, GenerateAnswer> numberGameMap = new HashMap<>();
	private AtomicInteger nextId = new AtomicInteger(0);
	
	@PostMapping("/api/games")
	public ResponseEntity<String> creatGames() {
		Integer gameId = nextId.getAndIncrement();
		numberGameMap.put(gameId, new GenerateAnswer());
		return ResponseEntity.status(HttpStatus.CREATED)
			.header("Location", "/api/games/" + gameId.toString())
			.build();
	}
	
	@GetMapping("/api/games/{gameId}")
	public ResponseEntity<NumberGameInfo> getGameStatus(@PathVariable Integer gameId) {
		GenerateAnswer generateAnswer = numberGameMap.get(gameId);
		if (generateAnswer != null) {
			NumberGameInfo numberGameInfo = new NumberGameInfo(gameId, generateAnswer.getAnswer().getAnswer());
			return ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.body(numberGameInfo);
		}
		throw new IllegalArgumentException();
	}
	
	@ExceptionHandler
	public ResponseEntity<String> nullPintAndArithmeticException(IllegalArgumentException e) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	
	@PatchMapping("/api/games/{gameId}")
	public ResponseEntity<ResponseResult> patchGuessNumber(@PathVariable Integer gameId,
	                                                       @RequestBody @Valid GameAnswer gameAnswer) {
		
		String pattern = "[0-9]{4}";
		String content = gameAnswer.getAnswer();
		if (!Pattern.matches(pattern, content)) {
			return ResponseEntity.status(400).body(null);
		}
		if (!this.numberGameMap.containsKey(gameId)) {
			return ResponseEntity.status(404).body(null);
		}
		GenerateAnswer generateAnswer = this.numberGameMap.get(gameId);
		ResponseResult responseResult = generateAnswer.getAnswer().checkAnswer(gameAnswer);
		
		return ResponseEntity.ok()
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.body(responseResult);
	}
}

package com.twuc.webApp.web;

public class ResponseResult {
	private String hint = "";
	private boolean correct;
	
	public String getHint() {
		return hint;
	}
	
	public boolean isCorrect() {
		return correct;
	}
	
	public ResponseResult() {
	}
	
	public ResponseResult(String hint, boolean correct) {
		if(hint == null){
			this.hint = "0A0B";
			this.correct = false;
		} else {
			this.hint = hint;
			this.correct = correct;
		}
	}
}

package com.twuc.webApp.web;

public class NumberGameInfo {
	private Integer id;
	private String answer;
	
	public NumberGameInfo() {
	}
	
	public NumberGameInfo(Integer id, String answer) {
		this.id = id;
		this.answer = answer;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getAnswer() {
		return answer;
	}
}

package com.twuc.webApp.web;

import javax.validation.constraints.Pattern;

public class GameAnswer {
	
	private  String answer;
	
	public GameAnswer() {
		this.answer = "";
	}
	
	public GameAnswer(String answer) {
		this.answer = answer;
	}
	
	public String getAnswer() {
		return answer;
	}
	
	public ResponseResult checkAnswer(GameAnswer gameAnswer){
		StringBuilder stringBuilder = new StringBuilder();
		String correctAnswer = this.answer;
		String currentAnswer = gameAnswer.getAnswer();
		Integer countA = 0, countB = 0;
		for (int i = 0; i < 4; ++i){
			char curChar = currentAnswer.charAt(i);
			char corChar = correctAnswer.charAt(i);
			if(curChar == corChar){
				countA++;
			}else if(correctAnswer.contains(Character.toString(curChar))){
				countB++;
			}
		}
		stringBuilder.append(countA);
		stringBuilder.append("A");
		stringBuilder.append(countB);
		stringBuilder.append("B");
		if(stringBuilder.toString().equals("4A0B")){
			return new ResponseResult(stringBuilder.toString(), true);
		}
		return new ResponseResult(stringBuilder.toString(), false);
	}
	
}

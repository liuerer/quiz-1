package com.twuc.webApp.web;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class GenerateAnswer {
    private GameAnswer answer;
    
    public GenerateAnswer() {
        int cur;
        StringBuilder answerString = new StringBuilder();
        Random random = new Random();
        Set<Integer> numberSet = new HashSet<>();
        for(int i = 0; i < 4; ++i) {
            do {
                cur = random.nextInt(10);
            }while (numberSet.contains(cur));
            numberSet.add(cur);
            answerString.append(cur);
        }
    
        this.answer = new GameAnswer(answerString.toString());
    }
    
    public GameAnswer getAnswer() {
        return answer;
    }
}
